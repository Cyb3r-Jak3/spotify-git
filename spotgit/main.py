""" Module that allows for management of spotify playlists"""
import json
import requests
import utils


API_URL = "https://api.spotify.com/v1/"


access = utils.oauth()
headers = {"Authorization": "Bearer {}".format(access["access_token"])}


def get_playlists():
    """Returns playlists that the user follows"""
    r = requests.get(API_URL + "me/playlists?offset=0&limit=50",
                     headers=headers).json()
    # for x in r["items"]:
#    print(x["name"], x["id"])
    return r


def get_tracks(playlist_id: str):
    """Gets all the tracks in a playlist"""
    tracks = requests.get(API_URL +
                          "playlists/{playlist_id}/tracks".format(
                              playlist_id=playlist_id)
                          + "?fields=(items(track(name, id, uri)))",
                          headers=headers).json()
    return tracks["items"]


playlist_tracks = get_tracks("6XAFxzkYnVcsShE3zJxdLZ")
# print(playlist_tracks)
# for track in playlist_tracks:
#    print(track["track"])


def write_playlistfile(playlist: str, tracks: list):
    """write_playlistfile

    Arguments:
        playlist {str} -- ID of the playlist
        tracks {list} -- track information for the playlist
    """
    playlist_file = open(playlist + ".playlist", "w")
    for track in tracks:
        playlist_file.write(json.dumps(track))


write_playlistfile("6XAFxzkYnVcsShE3zJxdLZ", playlist_tracks)
