""" Module that allows for management of spotify playlists"""
import base64
import json
import webbrowser
import os
import secrets
import string
import time
import requests
from dotenv import load_dotenv

load_dotenv()


# Bandit throws warning about possible hardcoded password.
TOKEN_URL = "https://accounts.spotify.com/api/token"  # nosec: B105


class TokenError(Exception):
    """Error that gets raise if the Token fails"""


def state_gen(stringLength: int):
    """Makes a random string that is used for the state"""
    characters = string.ascii_letters + string.digits
    return ''.join(secrets.choice(characters) for i in range(stringLength))


def make_authorization_headers(client_id: str, client_secret: str):
    """Base64 encodes the client id and secret"""
    auth_header = base64.b64encode(
        (client_id +
         ':' +
         client_secret).encode("utf-8"))
    return {'Authorization': 'Basic %s' % auth_header.decode("utf-8")}


def add_expiration(token: dict):
    """Adds an expiration time for the token"""
    token["expires"] = int(time.time()) + token["expires_in"]
    return token


def check_expiration(token: dict):
    """Checks to see if the access token has expired"""
    now = int(time.time())
    return token["expires"] - now < 60


def login(dialog: str = "true"):
    """Gets the login url and opens it"""
    state = state_gen(20)
    base_login_url = "https://accounts.spotify.com/authorize?"
    login_url = (base_login_url + "client_id=" + os.getenv("CLIENT_ID")
                 + "&response_type=code"
                 + "&redirect_uri=https://spot-git.jwhite.network/"
                 + "&scope=playlist-modify-private%20playlist-read-private%20"
                   "playlist-modify-public%20playlist-read-collaborative"
                 + "&show_dialog={}".format(dialog)
                 + "&state={}".format(state))
    r = requests.get(login_url)
    webbrowser.open_new_tab(r.url)


def access(code):
    """Gets the access token"""
    headers = make_authorization_headers(
        os.getenv("CLIENT_ID"),
        os.getenv("CLIENT_SECRET")
        )
    payload = {
        "redirect_uri": "https://spot-git.jwhite.network/",
        "code": code,
        "grant_type": "authorization_code"
    }
    print(code)
    r = requests.post(TOKEN_URL, data=payload,
                      headers=headers, verify=True).json()
    if "error" in r:
        print(r)
        raise TokenError("There was an error geting the token.")
    print(r)
    token = add_expiration(r)
    try:
        f = open("token", "w")
        f.write(json.dumps(token))
        f.close()
        return token
    except IOError:
        print("Error: Could not save the token.")


def refresh(token):
    """Gets an access token using the refresh token"""
    headers = make_authorization_headers(
        os.getenv("CLIENT_ID"),
        os.getenv("CLIENT_SECRET")
    )
    payload = {
        "refresh_token": token["refresh_token"],
        "grant_type": "refresh_token"
    }
    r = requests.post(TOKEN_URL, data=payload, headers=headers).json()
    token = add_expiration(r)

    try:
        f = open("token", "w")
        f.write(json.dumps(token))
        f.close()
        return token
    except IOError:
        print("Error: Could not save the token.")


def oauth():
    """
    Main function for manageing oauth. Returns the access information as json
    """
    try:
        f = open("token", "r")
        token = json.loads(f.read())
        if check_expiration(token):
            refresh(token)
        f.close()
        return token
    except IOError:
        login()
        # Bandit throws warning about input.
        # Only appliable for python 2.
        code = input("Please enter the code you received.\n >")  # nosec: B321
        return access(code)
