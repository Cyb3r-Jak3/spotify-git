"""Sets up Spot-git to be installed"""
import os
from setuptools import setup
from spotgit import __version__


def read(fname):
    """Reads README.md as long description"""
    return open(os.path.join(os.path.dirname(__file__), fname)).read()


setup(
    name="spot-git",
    version=__version__,
    author="Cyb3r Jak3",
    author_email="jake@jwhite.network",
    install_requires=['requests'],
    description="Git style managment for Spotify playlists.",
    license="MPL 2.0",
    python_requires=">=3.6",
    classifiers=[
        "License :: OSI Approved :: Mozilla Public License 2.0 (MPL 2.0)",
        "Operating System :: OS Independent",
        "Natural Language :: English",
        "Programming Language :: Python :: 3 :: Only",
        "Development Status :: 1 - Planning"
    ],
    long_description=read('README.md'),
    long_description_content_type='text/markdown',
    entry_points={
        "console_scripts": ["spot-git=spotgit.main:run"]
    }
)
